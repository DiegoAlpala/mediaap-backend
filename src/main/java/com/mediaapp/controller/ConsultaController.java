package com.mediaapp.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mediaapp.dto.ConsultaListaExamenDTO;
import com.mediaapp.dto.ConsultaResumenDTO;
import com.mediaapp.dto.FiltroConsultaDTO;
import com.mediaapp.model.Archivo;
import com.mediaapp.model.Consulta;
import com.mediaapp.service.IArchivoService;
import com.mediaapp.service.IConsultaService;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

	@Autowired
	private IConsultaService service;
	
	@Autowired
	private IArchivoService serviceArchivo;

	@GetMapping
	public ResponseEntity<List<Consulta>> listarConsultas() {
		List<Consulta> lista = service.listar();
		return new ResponseEntity<List<Consulta>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Consulta> listarPorId(@PathVariable("id") Integer id) {
		Consulta consulta = service.listarPorId(id);
		return new ResponseEntity<Consulta>(consulta, HttpStatus.OK);
	}
	
	/* 
	 * Servicio usando dependencia Hateoas para devolver el recurso
	 * */
	@GetMapping("/hateoas/{id}")
	public EntityModel<Consulta> listarPorIdHateoas(@PathVariable("id") Integer id){
		Consulta consulta = service.listarPorId(id);
		EntityModel<Consulta> recurso = new EntityModel<Consulta>(consulta);
		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		recurso.add(linkTo.withRel("consulta-resource"));
		return recurso;
	}


	@PostMapping
	public ResponseEntity<Consulta> registrar(@Valid @RequestBody ConsultaListaExamenDTO dto) {
		Consulta obj = service.registrarTransaccional(dto);
		return new ResponseEntity<Consulta>(obj, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Consulta> modificar(@Valid @RequestBody Consulta consulta) {
		Consulta obj = service.modificar(consulta);
		return new ResponseEntity<Consulta>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	
	@PostMapping("/buscar")
	public ResponseEntity<List<Consulta>> buscar( @RequestBody FiltroConsultaDTO filtro) {
		List<Consulta> consultas = new ArrayList<>();
		
		if(filtro != null) {
			if(filtro.getFechaConsulta() != null) {
				consultas = service.buscarFecha(filtro);
				
			}else {
				consultas = service.buscar(filtro);
			}
		}
		
		return new ResponseEntity<List<Consulta>>(consultas, HttpStatus.CREATED);
	}
	
	@GetMapping(value="/listarResumen")
	public ResponseEntity<List<ConsultaResumenDTO>> listarResumen() {
		
		List<ConsultaResumenDTO> consultas = new ArrayList<>();
		consultas = service.listraResumen();
		return new ResponseEntity<List<ConsultaResumenDTO>>(consultas, HttpStatus.OK);
	}
	
	//MediaType.APPLICATION_OCTET_STREAM_VALUE permite enviar el arreglo de bits en lugar de json
	@GetMapping(value="/generarReporte", produces= MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporte() {
		byte[] data= null;
		data = service.generarReporte();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	//Método para guardar el archivo en base de datos el mediaType corresponde al tipo de dato enviado desde el frontend
	@PostMapping(value="/guardarArchivo", consumes= {MediaType.MULTIPART_FORM_DATA_VALUE} )
	public  ResponseEntity<Integer> guardarArchivo(@RequestParam("adjunto") MultipartFile file) throws IOException {
		int rpta = 0;
		Archivo ar =  new Archivo();
		ar.setFilename(file.getName());
		ar.setFiletype(file.getContentType());
		ar.setValue(file.getBytes());
		
		rpta = serviceArchivo.guardar(ar);
		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
	
	//MediaType.APPLICATION_OCTET_STREAM_VALUE sirve para devlover un arreglo de bytes.
	@GetMapping(value="/leerArchivo/{idArchivo}", produces= MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> leerArchivo(@PathVariable("idArchivo") Integer idArchivo) {
		byte[] arr= null;
		arr = serviceArchivo.leerArchivo(idArchivo);
		return new ResponseEntity<byte[]>(arr, HttpStatus.OK);
	}

}
