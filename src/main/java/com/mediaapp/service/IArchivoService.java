package com.mediaapp.service;

import com.mediaapp.model.Archivo;

public interface IArchivoService {

	int guardar(Archivo archivo);
	byte[] leerArchivo(int id);
}
