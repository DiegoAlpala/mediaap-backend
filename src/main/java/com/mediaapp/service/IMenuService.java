package com.mediaapp.service;

import java.util.List;

import com.mediaapp.model.Menu;

public interface IMenuService extends ICRUD<Menu, Integer>{

	List<Menu> listarMenuPorUsuario(String usuario);
}
