package com.mediaapp.service;

import com.mediaapp.model.Usuario;

public interface ILoginService {

	Usuario verificarNombreUsuario(String usuario);
	
	void cambiarClave(String clave, String usuario);
}
