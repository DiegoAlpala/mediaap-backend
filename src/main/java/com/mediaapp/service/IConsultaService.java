package com.mediaapp.service;


import java.util.List;

import com.mediaapp.dto.ConsultaListaExamenDTO;
import com.mediaapp.dto.ConsultaResumenDTO;
import com.mediaapp.dto.FiltroConsultaDTO;
import com.mediaapp.model.Consulta;

public interface IConsultaService extends ICRUD<Consulta, Integer>{

	Consulta registrarTransaccional(ConsultaListaExamenDTO dto);
	
	List<Consulta> buscar(FiltroConsultaDTO filtro);
	
	List<Consulta> buscarFecha(FiltroConsultaDTO filtro);
	
	List<ConsultaResumenDTO> listraResumen();
	
	byte[] generarReporte();
	
}
