package com.mediaapp.service;

import java.util.List;

import com.mediaapp.model.ConsultaExamen;

public interface IConsultaExamenService {
	List<ConsultaExamen> listarExamenesPorConsulta(Integer idConsulta);
}
