package com.mediaapp.dto;

import java.util.List;

import com.mediaapp.model.Consulta;
import com.mediaapp.model.Examen;

public class ConsultaListaExamenDTO {

	private Consulta consulta;
	
	private List<Examen> listaExamen;

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public List<Examen> getListaExamen() {
		return listaExamen;
	}

	public void setListaExamen(List<Examen> listaExamen) {
		this.listaExamen = listaExamen;
	}
	
	
}
