package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Archivo;

public interface IArchivoRepo extends JpaRepository<Archivo, Integer>{

}
