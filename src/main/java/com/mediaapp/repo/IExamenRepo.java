package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Examen;

//@Repository no es necesario por que extiende de JpaRepository
public interface IExamenRepo extends JpaRepository<Examen, Integer> {

}
