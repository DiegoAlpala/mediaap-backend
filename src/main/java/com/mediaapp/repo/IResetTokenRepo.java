package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.ResetToken;

public interface IResetTokenRepo extends JpaRepository<ResetToken, Integer>{
	
	ResetToken findByToken(String token);

}
