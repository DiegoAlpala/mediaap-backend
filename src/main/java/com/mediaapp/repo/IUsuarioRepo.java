package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>{

	Usuario findOneByUsername(String name);
}
