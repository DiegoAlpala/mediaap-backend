package com.mediaapp.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mediaapp.model.Consulta;

//@Repository no es necesario por que extiende de JpaRepository
public interface IConsultaRepo extends JpaRepository<Consulta, Integer> {

	@Query("from Consulta c where c.paciente.dni=:dni or LOWER(c.paciente.nombres) like %:nombreCompleto% or LOWER(c.paciente.apellidos) like %:nombreCompleto% ")
	List<Consulta>buscar(@Param("dni")String dni, @Param("nombreCompleto")String nombreCompleto);
	
	@Query("from Consulta c where c.fecha between :fechaConsulta and :fechaSiguiente ")
	List<Consulta>buscarFecha(@Param("fechaConsulta")LocalDateTime fechaConsulta, @Param("fechaSiguiente")LocalDateTime fechaSiguiente);
	
	@Query(value="select * from fn_listarResumen()", nativeQuery=true)
	List<Object[]> listarResumen();
}
