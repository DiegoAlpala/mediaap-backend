package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Especialidad;

//@Repository no es necesario por que extiende de JpaRepository
public interface IEspecialidadRepo extends JpaRepository<Especialidad, Integer> {

}
