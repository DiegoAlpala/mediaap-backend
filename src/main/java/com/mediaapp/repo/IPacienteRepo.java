package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;

import com.mediaapp.model.Paciente;

//@Repository no es necesario por que extiende de JpaRepository
public interface IPacienteRepo extends JpaRepository<Paciente, Integer> {

}
