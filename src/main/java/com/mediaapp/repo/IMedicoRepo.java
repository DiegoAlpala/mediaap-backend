package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Medico;

//@Repository no es necesario por que extiende de JpaRepository
public interface IMedicoRepo extends JpaRepository<Medico, Integer> {

}
