package com.mediaapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaapp.model.Signos;

public interface ISignosRepo extends JpaRepository<Signos, Integer> {

}
