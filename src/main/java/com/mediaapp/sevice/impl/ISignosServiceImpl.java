package com.mediaapp.sevice.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Signos;
import com.mediaapp.repo.ISignosRepo;
import com.mediaapp.service.ISignosService;

@Service
public class ISignosServiceImpl implements ISignosService {

	@Autowired
	private ISignosRepo repo;
	
	@Override
	public Signos registrar(Signos obj) {
		return repo.save(obj);
	}

	@Override
	public Signos modificar(Signos obj) {
		return repo.save(obj);
	}

	@Override
	public List<Signos> listar() {
		return repo.findAll();
	}

	@Override
	public Signos listarPorId(Integer id) {
		Optional<Signos> signos = repo.findById(id);
		return signos.isPresent() ? signos.get() : null;
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
