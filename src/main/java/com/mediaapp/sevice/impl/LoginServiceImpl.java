package com.mediaapp.sevice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Usuario;
import com.mediaapp.repo.ILoginRepo;
import com.mediaapp.service.ILoginService;

@Service
public class LoginServiceImpl implements ILoginService {

	@Autowired
	private ILoginRepo repo;
	
	@Override
	public Usuario verificarNombreUsuario(String usuario) {
		
		return repo.verificarNombreUsuario(usuario);
	}

	@Override
	public void cambiarClave(String clave, String usuario) {
		repo.cambiarClave(clave, usuario);
	}

}
