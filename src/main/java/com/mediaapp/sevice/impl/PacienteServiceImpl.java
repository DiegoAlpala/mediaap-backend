package com.mediaapp.sevice.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Paciente;
import com.mediaapp.repo.IPacienteRepo;
import com.mediaapp.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService {

	@Autowired
	private IPacienteRepo repo;
	
	@Override
	public Paciente registrar(Paciente paciente) {
		return repo.save(paciente);
	}

	@Override
	public Paciente modificar(Paciente paciente) {
		return repo.save(paciente);
	}

	@Override
	public List<Paciente> listar() {
		return repo.findAll();
	}

	@Override
	public Paciente listarPorId(Integer id) {
		Optional<Paciente> paciente = repo.findById(id);
		return paciente.isPresent() ? paciente.get() : null;
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

	@Override
	public Page<Paciente> listarPageable(Pageable pageable) {
		
		return repo.findAll(pageable);
	}

}
