package com.mediaapp.sevice.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Archivo;
import com.mediaapp.repo.IArchivoRepo;
import com.mediaapp.service.IArchivoService;

@Service
public class ArchivoServiceImpl implements IArchivoService {

	@Autowired
	private IArchivoRepo repo;
	
	@Override
	public int guardar(Archivo archivo) {
		Archivo ar = repo.save(archivo);		
		return ar.getIdArchivo()>0?1:0;
	}

	@Override
	public byte[] leerArchivo(int id) {
		Optional<Archivo> op = repo.findById(id);
		return op.isPresent() ? op.get().getValue() : new byte[0];
	}

}
