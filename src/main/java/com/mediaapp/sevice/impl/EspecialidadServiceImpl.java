package com.mediaapp.sevice.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Especialidad;
import com.mediaapp.repo.IEspecialidadRepo;
import com.mediaapp.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

	@Autowired
	private IEspecialidadRepo repo;
	
	@Override
	public Especialidad registrar(Especialidad especialidad) {
		return repo.save(especialidad);
	}

	@Override
	public Especialidad modificar(Especialidad especialidad) {
		return repo.save(especialidad);
	}

	@Override
	public List<Especialidad> listar() {
		return repo.findAll();
	}

	@Override
	public Especialidad listarPorId(Integer id) {
		Optional<Especialidad> especialidad = repo.findById(id);
		return especialidad.isPresent() ? especialidad.get() : new Especialidad();
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
