package com.mediaapp.sevice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.ConsultaExamen;
import com.mediaapp.repo.IConsultaExamenRepo;
import com.mediaapp.service.IConsultaExamenService;

@Service
public class ConsultaExamenServiceImpl implements IConsultaExamenService {

	@Autowired
	private IConsultaExamenRepo repo;
	
	@Override
	public List<ConsultaExamen> listarExamenesPorConsulta(Integer idConsulta) {
	
		return repo.listarExamenesPorConsulta(idConsulta);
	}

}
