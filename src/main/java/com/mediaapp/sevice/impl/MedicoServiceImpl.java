package com.mediaapp.sevice.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Medico;
import com.mediaapp.repo.IMedicoRepo;
import com.mediaapp.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService {

	@Autowired
	private IMedicoRepo repo;
	
	@Override
	public Medico registrar(Medico medico) {
		return repo.save(medico);
	}

	@Override
	public Medico modificar(Medico medico) {
		return repo.save(medico);
	}

	@Override
	public List<Medico> listar() {
		return repo.findAll();
	}

	@Override
	public Medico listarPorId(Integer id) {
		Optional<Medico> medico = repo.findById(id);
		return medico.isPresent() ? medico.get() : new Medico();
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
