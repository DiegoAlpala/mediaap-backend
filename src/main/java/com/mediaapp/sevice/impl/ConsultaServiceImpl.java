package com.mediaapp.sevice.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.mediaapp.dto.ConsultaListaExamenDTO;
import com.mediaapp.dto.ConsultaResumenDTO;
import com.mediaapp.dto.FiltroConsultaDTO;
import com.mediaapp.model.Consulta;
import com.mediaapp.repo.IConsultaExamenRepo;
import com.mediaapp.repo.IConsultaRepo;
import com.mediaapp.service.IConsultaService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ConsultaServiceImpl implements IConsultaService {

	@Autowired
	private IConsultaRepo repo;
	
	@Autowired
	private IConsultaExamenRepo rcRepo;
	
	@Override
	public Consulta registrar(Consulta consulta) {
		/* Por referencia de memoria se va a insertar el elemento consulta en la tabla detalle_consulta*/
		consulta.getDetalleConsulta().forEach(det -> {
			det.setConsulta(consulta);
		});	
		
		return repo.save(consulta);
	}

	@Override
	public Consulta modificar(Consulta consulta) {
		return repo.save(consulta);
	}

	@Override
	public List<Consulta> listar() {
		return repo.findAll();
	}

	@Override
	public Consulta listarPorId(Integer id) {
		Optional<Consulta> consulta = repo.findById(id);
		return consulta.isPresent() ? consulta.get() : new Consulta();
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

	@Transactional //Si una operación falla se hace un rollBack de las transacciones
	@Override
	public Consulta registrarTransaccional(ConsultaListaExamenDTO dto) {
		
		dto.getConsulta().getDetalleConsulta().forEach(det -> {
			det.setConsulta(dto.getConsulta());
		});
		
		repo.save(dto.getConsulta());
		
		dto.getListaExamen().forEach(exa -> {
			rcRepo.registrar(dto.getConsulta().getIdConsulta(), exa.getIdExamen());
		});
		
		return dto.getConsulta();
	}

	@Override
	public List<Consulta> buscar(FiltroConsultaDTO filtro) {
		return repo.buscar(filtro.getDni(), filtro.getNombreCompleto());
	}

	@Override
	public List<Consulta> buscarFecha(FiltroConsultaDTO filtro) {
		LocalDateTime fechaSiguiente = filtro.getFechaConsulta().plusDays(1);
		return repo.buscarFecha(filtro.getFechaConsulta(), fechaSiguiente);
	}

	@Override
	public List<ConsultaResumenDTO> listraResumen() {
		List<ConsultaResumenDTO> consultas = new ArrayList<>();
		repo.listarResumen().forEach(x -> {
			ConsultaResumenDTO crd = new ConsultaResumenDTO();
			crd.setCantidad(Integer.parseInt(String.valueOf(x[0])));
			crd.setFecha(String.valueOf(x[1]));
			consultas.add(crd);
		});
		return consultas;
	}

	@Override
	public byte[] generarReporte() {
		byte [] data = null;
		try {
			File file = new ClassPathResource("/reports/consultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null,new JRBeanCollectionDataSource(this.listraResumen()));
			data = JasperExportManager.exportReportToPdf(print);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}
