package com.mediaapp.sevice.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaapp.model.Examen;
import com.mediaapp.repo.IExamenRepo;
import com.mediaapp.service.IExamenService;

@Service
public class ExamenServiceImpl implements IExamenService {

	@Autowired
	private IExamenRepo repo;
	
	@Override
	public Examen registrar(Examen examen) {
		return repo.save(examen);
	}

	@Override
	public Examen modificar(Examen examen) {
		return repo.save(examen);
	}

	@Override
	public List<Examen> listar() {
		return repo.findAll();
	}

	@Override
	public Examen listarPorId(Integer id) {
		Optional<Examen> examen = repo.findById(id);
		return examen.isPresent() ? examen.get() : new Examen();
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
