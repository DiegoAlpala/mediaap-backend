package com.mediaapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;

//@ApiModel(description = "Información del Paciente")
@Entity
@Table(name= "paciente")
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY  )
	private Integer idPaciente;
	
	//@ApiModelProperty(notes = "Nombres debe tener mínimo 3 caracteres") // para la documentacion del modelo
	@Size(min=3, message="Nombres debe tener mínimo 3 caracteres")
	@Column(name = "nombre", nullable = false, length = 512)
	private String nombres;
	
	@Size(min=4, message="Apellido debe tener mínimo 4 caracteres")
	@Column(name = "apellido", nullable = false, length = 512)
	private String apellidos;
	
	@Size(min=10, max=10, message="Cédula debe tener 10 caracteres")
	@Column(name = "dni", nullable = false, length = 13)
	private String dni;
	
	@Column(name = "direccion", nullable = false, length = 512)
	private String direccion;
	
	@Email
	@Column(name = "email", nullable = false, length = 128)
	private String email;
	
	@Size(min=10, max=10, message="Teléfono debe tener 10 caracteres")
	@Column(name = "telefono", nullable = false, length = 16)
	private String telefono;

	public Integer getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
}
